#!/usr/bin/env perl

# Daniel Bowling <swaggboi@slackware.uk>
# Dec 2020

use strict;
use warnings;
use utf8;
use open qw{:std :utf8}; # Fix "Wide character in print" warning
use XML::LibXML;
use Mastodon::Client;
#use Data::Dumper;        # Uncomment for debugging

# Get creds file
my $dotfile =
    (-r "$ENV{PWD}/.pommCreds.xml")  ? "$ENV{PWD}/.pommCreds.xml"  :
    (-r "$ENV{HOME}/.pommCreds.xml") ? "$ENV{HOME}/.pommCreds.xml" :
    die "Can't locate dotfile!!";
# Open the creds file
my $dom = XML::LibXML->load_xml(location => $dotfile);

# Grab the values
my $instance      = $dom->findvalue('/creds/instance');
my $name          = $dom->findvalue('/creds/name');
my $client_id     = $dom->findvalue('/creds/client_id');
my $client_secret = $dom->findvalue('/creds/client_secret');
my $access_token  = $dom->findvalue('/creds/access_token');

# Put the values into hash
my %conf = (
    instance        => $instance,
    name            => $name,
    client_id       => $client_id,
    client_secret   => $client_secret,
    access_token    => $access_token,
    coerce_entities => 1
    );

# Create new Mastodon::Client object
my $client = Mastodon::Client->new(%conf);

# Take a stab at the path for pom command
my $pomPath =
    (-x "/usr/games/pom") ? "/usr/games/pom" : "pom";
chomp(my $pom = `$pomPath`) || die $!;

# Add emoji to match phase
$pom =
    /The Moon is New/                  ? "$pom 🌚" :
    /The Moon is Waxing Crescent/      ? "$pom 🌒" :
    /The Moon is at the First Quarter/ ? "$pom 🌛" :
    /The Moon is Waxing Gibbous/       ? "$pom 🌔" :
    /The Moon is Full/                 ? "$pom 🌝" :
    /The Moon is Waning Gibbous/       ? "$pom 🌖" :
    /The Moon is at the Last Quarter/  ? "$pom 🌜" :
    /The Moon is Waning Crescent/      ? "$pom 🌘" :
    $_ for $pom;

# Send it
#$client->post_status($pom); # To post publically on fedi
$client->post_status($pom, {visibility => 'unlisted'});
