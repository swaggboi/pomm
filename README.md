# pomm

Mastodon Bot script I run via cron to post the phase of moon (using
the pom command)

_pomm eq "Phase of Moon Mastodon";_

pommm wants to run alongside a dot file:

- .pommCreds.xml in same directory or $HOME/.pommCreds.xml

    The Mattermost credentials to use

This script relies on XML::LibXML, Mastodon::Client (alongside
its many dependencies) and you'll need the `pom` command to be
available at `/usr/games/pom`.

## Credentials

Create a new account for your bot; set an avatar and profile per usual
but make sure to note somewhere who owns the bot. While in your
profile settings, click on "Development". You should see an empty list
of "applications"; click on "NEW APPLICATION". The only thing you'll
need to enter is an application name then hit "SUBMIT". Now, back at
your list of applications, click on the application you just created
and you’ll see some long alphanumeric strings. These are your secret
tokens, go ahead and place these values into your .pommCreds.xml file
(use "client key" as `client_id`).

## The `pom` Command

The `pom` command typically comes bundled with a package called `bsdgames`:

### Slackware

    root@optepr0n:~/git_repos/pom_swagg# slackpkg file-search pom
    
    
    NOTICE: pkglist is older than 24h; you are encouraged to re-run 'slackpkg update'
    
    Looking for pom in package list. Please wait... /-\|DONE
    
    The list below shows the packages that contains "pom" file.
    
    [ Status           ] [ Repository               ] [ Package                                  ]
       installed               slackware64                  bsd-games-2.13-x86_64-12                  
    
    You can search specific packages using "slackpkg search package".

### Debian

    root@timecube:~# apt-file find /usr/games/pom
    bsdgames: /usr/games/pom
